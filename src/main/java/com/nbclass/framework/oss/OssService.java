package com.nbclass.framework.oss;

import com.nbclass.framework.util.DateUtil;
import com.nbclass.framework.util.UUIDUtil;
import com.nbclass.vo.CloudStorageConfigVo;
import org.apache.commons.lang.StringUtils;

import java.io.InputStream;
import java.util.Date;
import java.util.UUID;

/**
 * 云存储公共服务类
 */
public abstract class OssService {
    /** 云存储配置信息 */
    CloudStorageConfigVo config;

    /**
     * 文件路径
     * @param prefix 前缀
     * @param suffix 后缀
     * @return 返回上传路径
     */
    String getPath(String prefix, String suffix) {
        //生成uuid
        String uuid = UUIDUtil.generateShortUuid();
        //文件路径
        String path = DateUtil.format(new Date(), "yyyyMMdd") + "/" + uuid;

        if(StringUtils.isNotBlank(prefix)){
            if (prefix.endsWith("/")){
                path = prefix +  path;
            }else{
                path = prefix + "/" + path;
            }

        }

        return path + suffix;
    }

    /**
     * 文件上传
     * @param data    文件字节数组
     * @param path    文件路径，包含文件名
     * @return        返回http地址
     */
    public abstract String upload(byte[] data, String path, boolean isPublic);

    /**
     * 文件上传
     * @param data     文件字节数组
     * @param suffix   后缀
     * @return         返回http地址
     */
    public abstract String uploadSuffix(byte[] data, String suffix, boolean isPublic);

    /**
     * 文件上传
     * @param inputStream   字节流
     * @param path          文件路径，包含文件名
     * @return              返回http地址
     */
    public abstract String upload(InputStream inputStream, String path, boolean isPublic);

    /**
     * 文件上传
     * @param inputStream  字节流
     * @param suffix       后缀
     * @return             返回http地址
     */
    public abstract String uploadSuffix(InputStream inputStream, String suffix, boolean isPublic);

}
